<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// for blade views
Route::get('/user', 'userController@index')->name('users');
Route::get('/add-user','userController@create')->name('newuser');
Route::post('/add-user','userController@store')->name('addUser');


// for vue.js views
Route::get('/', function () {
    return view('userlist');
});

Route::get('/add-user-vue', function (){
	return view('adduservue');
});
