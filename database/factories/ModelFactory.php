<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->e164PhoneNumber,
        'gender' => (($faker->numberBetween($min=0, $max=1))== 0 ? 'Male':'Female'),
        'dob' => $faker->date,
        'biography' => $faker->text(100),
        'image_src' => 'http://ecx.images-amazon.com/images/I/51ZU%2BCvkTyL.jpg'

    ];
});
