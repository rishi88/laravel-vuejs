
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

// var VueRouter = require('vue-router');
// Vue.use(VueRouter);

//import Vue from 'vue';
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);


Vue.component('example', require('./components/Example.vue'));
// Vue.component('users', require('./components/Users.vue'));
import Users from './components/Users.vue';

/*
const router = new Vuerouter({
	hashbang: false,
	base: __dirname,
	linkActiveClass: 'active',
	routes: [
		{path: '/example', component: Example, name: 'example'}
	]
});
*/

const app = new Vue({
    el: '#app',
    components: {Users}
});
