@extends('master')

@section('content')
		
		<div class="row">
			<div class="col-md-6 col-md-offset-3">

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3> 
							Users ({{$total}}) 
							<a class="pull-right" href="{{route('newuser')}}">Add User</a>
						</h3>

					</div>			
					@if(count($users)>0)
						@foreach($users as $user)
						<div class="panel-body">
							<ul class="list-group">
								<li class="list-group-item">
									<img src="{{$user['image_src']}}" height="100" width="75"/>
									{{$user['name']}} -- {{$user['email']}} -- {{$user['phone']}} 
								</li>
							</ul>
						</div>
						@endforeach
					<div style="margin-left: 32%;">
					{{ $users->links() }}
					</div>
				  @else
					<h3> No Users </h3>
				  @endif
				</div>
			</div>
		</div>

@endsection
